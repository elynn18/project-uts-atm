# kelas utama
class Pertama:
    #inisial metode / penampungan data
    def __init__(self, nama, umur, kelamin):
        self.name = nama
        self.age = umur
        self.kelamin = kelamin
    # metode buatan
    def gojosatoru(self):
        print(f"nama saya {self.name} umur saya {self.age} jenis kelamin {self.kelamin}")

#deklarasi objek
penyihir = Pertama("gojo", 25, "lakilaki")
penyihir.gojosatoru()

#pewarisan
class Kedua(Pertama):
     def __init__(self, nama, umur, kelamin):
        Pertama.__init__(self, nama, umur, kelamin)

jutsu = Kedua("sukuna", 23, "lakilaki")
jutsu.gojosatoru()

