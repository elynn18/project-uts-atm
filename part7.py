# Variable local dan global
a1 = 0 # ini bersifat global


def hitung():
    global a1
    # ini bersifat lokal
    a1 = 10
    print(a1)


hitung()
print(a1)
hitung()

# variable private dan protected
class Pahlawan:
    def __init__(self, nama, kekuatan):
        # variable private
        self.__nama = nama
        # variable protected
        self._kekuatan = kekuatan

    # Getter

    def getNama(self):
        return self.__nama
    
    # Setter

    def gantiNama(self, name):
        self.__nama = name
        return self.__nama
    
dul = Pahlawan('Dulla', 'Terbang')
print(dul.__dict__)
#print(dul.__nama)
print(dul.getNama())
print(dul.gantiNama('Somad'))

